package threaduri;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Interfata {
	private JTextArea ta = new JTextArea();
	
	private JFrame frame = new JFrame("Simple Frame");
	private JPanel panel = new JPanel();
	private JLabel l6 = new JLabel("MaxSerTime");
	private JLabel l5 = new JLabel("MinSerTime");
	private JLabel l1 = new JLabel("MinArival");
	private JLabel l2 = new JLabel("MaxArival");
	private JLabel l9 = new JLabel("Queues");
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	private JTextField minSerTime = new JTextField();
	private JTextField maxSerTime = new JTextField();
	private JTextField minArrTime = new JTextField();
	private JTextField maxArrTime = new JTextField();
	private JTextField simTime = new JTextField();
	JTextField tf31 = new JTextField();
	private String s;
	
	public Interfata() {
		// INTERAFATA

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 500);
		frame.setVisible(true);
		panel.setLayout(null);
		panel.setBounds(0, 0, 700, 500);
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		panel.setBackground(Color.white);
		l5.setBounds(525, 25, 75, 40);
		panel.add(l5);
		minSerTime.setBounds(560, 65, 50, 25);
		panel.add(minSerTime);
		l6.setBounds(525, 110, 75, 40);
		panel.add(l6);
		maxSerTime.setBounds(560, 150, 50, 25);
		panel.add(maxSerTime);
		l1.setBounds(525, 195, 75, 40);
		panel.add(l1);
		minArrTime.setBounds(560, 235, 50, 25);
		panel.add(minArrTime);
		l2.setBounds(525, 280, 75, 40);
		panel.add(l2);
		maxArrTime.setBounds(560, 320, 50, 25);
		panel.add(maxArrTime);
		
		
		l9.setBounds(525, 365, 75, 40);
		panel.add(l9);
		
		tf31.setBounds(560, 405, 50, 25);
		panel.add(tf31);
		s = tf31.getText();
		frame.add(panel);
		panel.setBackground(Color.WHITE);
		ta.setFont(new Font("Serif", Font.ITALIC, 16));
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setBackground(Color.white);
		ta.setBounds(50, 100, 400, 300);
		panel.add(ta);
		
		start.setBounds(50, 20, 100, 30);
		panel.add(start);
		
		stop.setBounds(350, 20, 100, 30);
		panel.add(stop);
		simTime.setFont(new Font("Serif", Font.ITALIC, 16));
		simTime.setAlignmentX(JTextField.CENTER);
		simTime.setBackground(Color.white);
		simTime.setBounds(225, 20, 50, 30);
		panel.add(simTime);
		
		frame.setVisible(true);
	}

	public String getTa() {
		return this.ta.getText();
	}
	public JTextArea getTa1() {
		return this.ta;
	}
	public void setTa(String ta) {
		this.ta.setText(ta);
	}

	public String getMinSerTime() {
		return this.minSerTime.getText();
	}

	public void setMinSerTime(int minSerTime) {
		this.minSerTime.setText(String.valueOf(minSerTime));
	
	}
	
	public String getMaxSerTime() {
		return maxSerTime.getText();
	}

	public void setMaxSerTime(int maxSerTime) {
		this.maxSerTime.setText(String.valueOf(maxSerTime));
	}

	public String getMinArrTime() {
		return minArrTime.getText();
	}

	public void setMinArrTime(int minArrTime) {
		this.minArrTime.setText(String.valueOf(minArrTime));;
	}

	public String getMaxArrTime() {
		return maxArrTime.getText();
	}

	public void setMaxArrTime(int i) {
		this.maxArrTime.setText(String.valueOf(i));
	}

	public String getSimTime() {
		return simTime.getText();
	}

	public void setSimTime(int simTime) {
		this.simTime.setText(String.valueOf(simTime));
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}
	public JButton getStart(){
		return this.start;
	}
	public JButton getStop(){
		return this.stop;
	}

	public int getNoQueues() {
		return Integer.parseInt(tf31.getText());
		
	}
}
