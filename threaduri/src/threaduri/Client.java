package threaduri;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class Client {
	private int serviceTime;
	private int arrivalTime;
	private int id = this.number1;
	private String state;
	public int st;
	public int startTime;
	public int endTime;
	public int waitingTime;
	public static int number1 = 1;

	public Client() {
	}

	public Client(int s, int a) {
		this.serviceTime = s;
		this.arrivalTime = a;
	}

	public synchronized int chooseQue(LinkedBlockingQueue<Coada> queues) {
		int choosen = 1;
		int minim = queues.peek().getSize();
		for (Coada c : queues) {
			// System.out.println(c.number);
			if (c.getSize() < minim) {
				minim = c.getSize();
				choosen = c.getNumber();
			}
		}
		return choosen;
	}

	public synchronized String toString() {
		return "(C" + this.id + " " + serviceTime + " " + arrivalTime + " )";
	}

	public synchronized String toString1() {
		return state + " (C" + this.id + " " + serviceTime + " " + arrivalTime + " )" + " ";
	}

	public int getServiceTime() {
		return this.serviceTime;
	}

	public int getArrivalTime() {
		return this.arrivalTime;
	}

	public void setSericeTime(int s) {
		this.serviceTime = s;

	}

	public void setArrivalTime(int a) {
		this.arrivalTime = a;
	}

	public synchronized String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
