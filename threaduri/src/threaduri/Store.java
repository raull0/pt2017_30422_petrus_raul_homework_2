package threaduri;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class Store implements Runnable {

	LinkedBlockingQueue<Coada> queues = new LinkedBlockingQueue<>();
	ArrayList<Thread> th = new ArrayList<>();
	private int minSerTime;
	private int maxSerTime;
	private int minArrTime;
	private int maxArrTime;
	fileCreater fc;
	private String s;
	private int simTime;
	public int simulationTimer;
	public static int j = 0;

	public Store() {
	}

	public Store(fileCreater fc) {
		this.fc = fc;
	}

	public synchronized Client generateClient(int minSerTime, int maxSerTime, int minArrTime, int maxArrTime) {
		Random r = new Random();
		Client c = new Client();
		int arr = r.nextInt(maxArrTime - minArrTime + 1) + minArrTime;
		int ser = r.nextInt(maxSerTime - minSerTime + 1) + minSerTime;
		c.setArrivalTime(arr);
		c.setSericeTime(ser);
		c.st = ser;
		c.startTime = j;

		c.setState(j / 60 + ":" + j % 60 + " " + "Has been added to " + c.chooseQue(queues) + " ");
		System.out.println(c.toString1());
		try {
			fc.addRecords(c.toString1());
		} catch (NullPointerException e) {
		}
		return c;

	}

	public void run() {
		simulationTimer = 0;

		while (simulationTimer <= simTime) {
			Client c = generateClient(this.getMinSerTime(), this.getMaxSerTime(), this.getMinArrTime(),
					this.getMaxArrTime());
			this.addC(c.chooseQue(queues), c);
			try {
				Thread.sleep(1000 * c.getArrivalTime());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			simulationTimer = simulationTimer + c.getArrivalTime();
			//Thread.yield();

		}
	}

	public void generateQueues(int n) {
		int j = 1;
		;
		for (int i = 0; i < n; i++) {
			Coada c = new Coada(j++, this.fc);
			queues.add(c);
			Thread t = new Thread(c);
			th.add(t);

		}
	}

	public synchronized void addC(int choosen, Client c) {
		for (Coada coada : this.queues) {
			if (coada.getNumber() == choosen)
				coada.addClient(c);

		}
	}

	public int avrWaitingTime() {
		int avrw8 = 0;
		int k = 0;
		for (Coada c : this.queues) {
			avrw8 = avrw8 + c.avrWaitingTime();

		}
		k = this.queues.size();
		avrw8 = avrw8 / k;
		System.out.println("The average waiting time for all clients is "+avrw8);
		return avrw8;
	}

	public int avrServiceTime() {
		int avrService = 0;
		for (Coada c : this.queues) {
			avrService = avrService + c.avrServiceTime();
		}
		avrService = avrService / this.queues.size();
		System.out.println("The average service time for all clinets is "+avrService);
		return avrService;
	}

	public synchronized void emptyTime() {
		for (Coada c : this.queues) {
			fc.addRecords("Empty Time for Queue" + c.getNumber() + " is: " + c.emptyTime());
		}
	}

	public synchronized String toString() {
		String s = " ";
		j++;

		for (Coada c : this.queues) {
			s = s + "\n";
			s = s + c.toString();
			s.concat("\n");
		}

		return s;
	}

	public int getMinSerTime() {
		return minSerTime;
	}

	public void setMinSerTime(int minSerTime) {
		this.minSerTime = minSerTime;
	}

	public int getMaxSerTime() {
		return maxSerTime;
	}

	public void setMaxSerTime(int maxSerTime) {
		this.maxSerTime = maxSerTime;
	}

	public int getMinArrTime() {
		return minArrTime;
	}

	public void setMinArrTime(int minArrTime) {
		this.minArrTime = minArrTime;
	}

	public int getMaxArrTime() {
		return maxArrTime;
	}

	public void setMaxArrTime(int maxArrTime) {
		this.maxArrTime = maxArrTime;
	}
	public void setSimTime(int simTime) {
		this.simTime = simTime;
	}


}
