package threaduri;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class Controller {
	fileCreater fc = new fileCreater();
	Store store = new Store(fc);
	Thread storeThread = new Thread(store);
	Interfata inter = new Interfata();
	Afisare afis = new Afisare(store, inter.getTa1());
	Thread display = new Thread(afis);
	
	public Controller() {
		inter.getStart().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {

					afis.setSimTime(Integer.parseInt(inter.getSimTime()));

					store.generateQueues(inter.getNoQueues());
					store.setMinArrTime(Integer.parseInt(inter.getMinArrTime()));
					store.setMaxArrTime(Integer.parseInt(inter.getMaxArrTime()));
					store.setMinSerTime(Integer.parseInt(inter.getMinSerTime()));
					store.setMaxSerTime(Integer.parseInt(inter.getMaxSerTime()));
					store.setSimTime(Integer.parseInt(inter.getSimTime()));
					storeThread.start();
					for (Coada c : store.queues)
						c.setSimTime(Integer.parseInt(inter.getSimTime()));
					for (Thread t : store.th)
						t.start();
					display.start();
					

				} catch (Exception e) {
				}
				;

			}
		});
		inter.getStop().addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				fc.addRecords("Average Waiting Time is: " + store.avrWaitingTime());
				fc.addRecords("Average Service Time is: " + store.avrServiceTime());
				store.emptyTime();
				display.stop();
				storeThread.stop();
				for (Thread t : store.th)
					t.stop();
				try {

					fc.closeFile();
				} catch (NullPointerException e) {
				}
				;
			}
		});
	}

}
