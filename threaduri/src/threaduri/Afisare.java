package threaduri;

import javax.swing.JTextArea;

public class Afisare implements Runnable {
	private Store store;
	private JTextArea ta;
	fileCreater fc;
	private int simulationTimer;
	private int simTime;

//	public Afisare(Store store) {
//		this.store = store;
//	}
//
//	public Afisare(Store store, fileCreater fc) {
//		this.fc = fc;
//		this.store = store;
//	}

	public Afisare(Store store, JTextArea ta) {

		this.store = store;
		this.ta = ta;
	}

	public synchronized void display() {
		int b = 0, c = 0;
		String s = " ";
		b = simulationTimer / 60;
		c = simulationTimer % 60;
		s = s + String.valueOf(b) + ":" + String.valueOf(c) + "\n";
		try {
			fc.addRecords(s);
		} catch (Exception e) {
		}

		ta.setText(s + " \n " + store.toString());
		for (Coada c1 : store.queues) {
			if (c1.isEmpty())
				c1.emptyTime++;
		}
	}

	@Override
	public void run() {
		simulationTimer = 0;

		while (simulationTimer <= simTime || this.Verify()) {
			this.display();
			try {

				Thread.sleep(1000);
				simulationTimer++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.display();

	}

	public synchronized boolean Verify() {
		boolean d = false;
		for (Coada c : store.queues) {
			if (c.clients.peek() != null)
				d = true;
		}
		return d;
	}

	public int getSimulationTimer() {
		return simulationTimer;
	}

	public void setSimulationTimer(int simulationTimer) {
		this.simulationTimer = simulationTimer;
	}

	public int getSimTime() {
		return simTime;
	}

	public void setSimTime(int simTime) {
		this.simTime = simTime;
	}

}
