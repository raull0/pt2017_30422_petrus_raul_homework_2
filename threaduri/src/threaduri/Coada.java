package threaduri;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

public class Coada implements Runnable {

	private int number;
	private int size;
	private int simTime;
	private int simulationTimer;
	fileCreater fc;
	public int emptyTime;

	public Coada(int i) {
		this.number = i;
	}

	public Coada(int i, fileCreater fc) {
		this.fc = fc;
		this.number = i;
	}

	LinkedBlockingQueue<Client> clients = new LinkedBlockingQueue<>();
	LinkedBlockingQueue<Client> allClients = new LinkedBlockingQueue<>();

	public void run() {
		while (simulationTimer <= simTime || this.clients.size() != 0) {
			if (clients.peek() != null) {
				if (clients.peek().getServiceTime() == 0) {
					clients.peek().endTime = Store.j;
					clients.peek()
							.setState(Store.j / 60 + ":" + Store.j % 60 + " Has been removed from " + this.number);
					fc.addRecords(clients.peek().toString1());
					System.out.println(clients.peek().toString1());
					this.clients.poll();
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

					}
					this.clients.peek().setSericeTime(this.clients.peek().getServiceTime() - 1);
				}

			}
		}
	}

	public synchronized void addClient(Client c) {
		Client.number1++;

		clients.add(c);
		allClients.add(c);
		this.size++;
	}

	public synchronized boolean isEmpty() {
		if (this.clients.isEmpty())
			return true;
		return false;
	}

	public synchronized String toString() {
		String s = "Coada" + this.number + ":";
		for (Client c : this.clients) {
			s = s + c.toString();
		}
		return s;
	}

	public int avrWaitingTime() {
		int avrw8 = 0;
		int k = 0;
		for (Client c : this.allClients) {
			avrw8 = avrw8 + c.endTime - c.startTime;
			System.out.println("C" + c.getId() + " end start " + c.endTime + " " + c.startTime);

		}
		k = this.allClients.size();
		try {
			avrw8 = avrw8 / k;
			System.out.println("Average waiting time for queue" + this.getNumber() + " is: " + avrw8);
		} catch (Exception e) {
		}
		;

		return avrw8;
	}

	public int avrServiceTime() {
		int avrService = 0;
		int k = 0;
		for (Client c : this.allClients) {
			avrService = avrService + c.st;

		}
		k = this.allClients.size();
		try {
			avrService = avrService / k;
		} catch (Exception e) {
		}
		;
		return avrService;
	}

	public int emptyTime() {
		return this.emptyTime;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public synchronized int getSimTime() {
		return simTime;
	}

	public void setSimTime(int simTime) {
		this.simTime = simTime;
	}

}
